package daos.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagers {

	private static EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("main.java.daos.impl");

	private EntityManagers() {
	}

	public static EntityManager createEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}

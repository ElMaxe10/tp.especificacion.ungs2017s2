package presentacion;

import services.UsuarioService;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.google.web.bindery.requestfactory.server.Pair;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public class PantallaRegistro extends VerticalLayout implements View {
	protected static final String NAME = "registro";

	// Componentes visuales
//	private FormLayout layout = new FormLayout();
	 private VerticalLayout layout = new VerticalLayout();
	private TextField userName = new TextField("Usuario");
	private TextField mail = new TextField("Mail");
	private TextField cuil = new TextField("CUIL");
	private TextField psw = new TextField("Password");
	private Button btnRegistrar = new Button("Registrar");
	private Button btnVolver = new Button("Volver");
	private Label error = new Label("No debe haber valores vacios");
	private Label error2 = new Label();
	private UsuarioService usuarioService = new UsuarioService();

	public PantallaRegistro() {
		addStyleName("fondo");
		construirLayout();
		clickBotones();
	}

	private void construirLayout() {
		setSizeFull();
		addComponent(layout);
		layout.setMargin(true);
		layout.setSpacing(true);
		userName.setValue("");
		mail.setValue("");
		cuil.setValue("");
		psw.setValue("");
		cuil.setMaxLength(11);
		error.setVisible(false);
		error2.setVisible(false);
		error.setWidth("200px");
		error2.setWidth("200px");
		error.addStyleName("v-label-failure");
		error2.addStyleName("v-label-failure");
		HorizontalLayout botones = new HorizontalLayout();
		botones.addComponents(btnRegistrar,btnVolver);
		layout.addComponents(userName,mail,cuil,psw,error,error2,botones);
		
	}

	private void clickBotones() {
		btnRegistrar.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				String userNameAux =userName.getValue();
				String pswAux= psw.getValue();
				String mailAux= mail.getValue();
				String cuilAux=cuil.getValue();
				if(!hayValoresEnBlanco(userNameAux,pswAux,mailAux,cuilAux)){
					error.setVisible(false);
					error2.setVisible(false);
					Pair<Boolean,String> tupla = usuarioService.crearUsuario(userNameAux, pswAux, mailAux, cuilAux);
					if (!tupla.getA()) {
						limpiarCampos();
						Notification.show(tupla.getB());
						moverHaciaPantallaLogin();
					} else {
						error2.setValue(tupla.getB());
						error2.setVisible(true);
						Notification.show(tupla.getB());
					}
				}

				else{
					Notification.show("No debe haber valores vacios");
					error.setVisible(true);
				}
			}

			private boolean hayValoresEnBlanco(String userNameAux,String pswAux, String mailAux, String cuilAux) {
				return userNameAux.equals("") || pswAux.equals("") || mailAux.equals("") || cuilAux.equals("");
			}
		});

		btnVolver.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				error.setVisible(false);
				error2.setVisible(false);
				moverHaciaPantallaLogin();
			}
		});

	}

	private void moverHaciaPantallaLogin() {
		getUI().getNavigator().navigateTo(PantallaLogin.NAME);
	}

	private void limpiarCampos() {
		userName.clear();
		mail.clear();
		cuil.clear();
		psw.clear();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// si quiero c�digo que se ejecute al ingresar.
	}
}

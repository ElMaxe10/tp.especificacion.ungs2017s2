package domain.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Post {

	@Id
	@GeneratedValue
	private Long id;
	private Date fechaCreacion;
	private String texto;

	@ManyToOne
	private Usuario usuario;
	private String userName;

	private int votosPositivos;
	private int votosNegativos;
	private int puntajeAcumulado;
	private double puntajePromedio;

	public Post() {
	}

	public Post(String texto, Usuario usuario, String userName) {
		setTexto(texto);
		setUsuario(usuario);
		setUserName(userName);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getVotosPositivos() {
		return votosPositivos;
	}

	public void setVotosPositivos(int votosPositivos) {
		this.votosPositivos = votosPositivos;
	}

	public int getVotosNegativos() {
		return votosNegativos;
	}

	public void setVotosNegativos(int votosNegativos) {
		this.votosNegativos = votosNegativos;
	}

	public int getPuntajeAcumulado() {
		return puntajeAcumulado;
	}

	public void setPuntajeAcumulado(int puntajeAcumulado) {
		this.puntajeAcumulado = puntajeAcumulado;
	}

	public double getPuntajePromedio() {
		return puntajePromedio;
	}

	public void setPuntajePromedio(double puntajePromedio) {
		this.puntajePromedio = puntajePromedio;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", usuario=" + userName + ", texto=" + texto + ",fechaCreacion=" + fechaCreacion
				+ ",VotosPositivos=" + votosPositivos + ",VotosNegativos=" + votosNegativos + ",Calificaci�nPromedio="
				+ puntajePromedio + "]";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

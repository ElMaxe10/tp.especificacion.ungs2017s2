package domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Calificacion {
	@Id
	@GeneratedValue
	private Long id;
	private int calificacion;
	@ManyToOne
	private Usuario usuario;
	@ManyToOne
	private Post post;
	private boolean votoPositivo;
	
	public Calificacion() {
	}
	
	public Calificacion(int calificacion, boolean voto) {
		setCalificacion(calificacion);
		setVotoPositivo(voto);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public boolean isVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(boolean votoPositivo) {
		this.votoPositivo = votoPositivo;
	}
	
}

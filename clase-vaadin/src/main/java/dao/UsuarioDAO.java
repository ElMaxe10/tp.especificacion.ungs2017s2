package dao;

import java.util.List;

import domain.model.Usuario;

public interface UsuarioDAO extends DAO<Usuario> {
	public Usuario getUsuarioPorId(Long idBuscado);

	public Usuario getUsuarioPorUserName(String userNameBuscado);

	public Usuario getUsuarioPorCuil(String cuilBuscado);

	public Usuario getUsuarioPorMail(String mailBuscado);

	public List<Usuario> getTodosLosUsuarios();
}

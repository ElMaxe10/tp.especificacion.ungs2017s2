package services;

import com.google.web.bindery.requestfactory.server.Pair;

import dao.UsuarioDAO;
import daos.impl.UsuarioDAOHibernate;
import domain.model.Usuario;

public class UsuarioService {

	private UsuarioDAO dao;
	private Usuario logueado;

	public UsuarioService() {
	}

	public void iniciarTransaccion() {
		dao = new UsuarioDAOHibernate();
	}

	public void cerrarTransaccion() {
		dao.cerrar();
	}

	public Usuario getLogueado() {
		return logueado;
	}

	public void setLogueado(Usuario logueado) {
		this.logueado = logueado;
	}

	public void guardar(Usuario usuario) {
		iniciarTransaccion();
		dao.guardar(usuario);
		cerrarTransaccion();
	}

	// para crear un usuario me fijo si ya hay un usuario con al menos uno de los
	// datos, de ser TRUE NO LO CREO y de vuelvo
	// una tupla con el true y un mensaje.
	public Pair<Boolean, String> crearUsuario(String userName, String password, String mail, String cuil_cuit) {
		String userNameMin = userName.toLowerCase();
		String passwordMin = password.toLowerCase();
		String mailMin = mail.toLowerCase();

		Pair<Boolean, String> tupla = comprobarExistenciaDeUsuario(userNameMin, mailMin, cuil_cuit);

		if (!tupla.getA()) {
			Usuario user = new Usuario(userNameMin, passwordMin, mailMin, cuil_cuit);
			guardar(user);
		}

		return tupla;
	}

	// Compruebo cada dato por separado para poder ser reutilizado cada metodo, si
	// existe un usuario guardado con alguno
	// de los datos genero un mensaje que dice que datos existen, sino se crea un
	// mensaje de creacion exitosa.
	private Pair<Boolean, String> comprobarExistenciaDeUsuario(String userName, String mail, String cuil_cuit) {
		boolean porUserName = ExisteUserName(userName);
		boolean porMail = ExisteMail(mail);
		boolean porCuil = ExistenciaCuil(cuil_cuit);

		String mensaje;
		boolean existe = porUserName || porMail || porCuil;

		if (existe)
			mensaje = generarMensaje(porUserName, porMail, porCuil);
		else
			mensaje = "Creacion de usuario exitosa";

		return new Pair<Boolean, String>(existe, mensaje);
	}

	private boolean ExisteUserName(String userName) {
		iniciarTransaccion();
		Usuario user = dao.getUsuarioPorUserName(userName);
		cerrarTransaccion();
		return user != null;
	}

	private boolean ExisteMail(String mail) {
		iniciarTransaccion();
		Usuario user = dao.getUsuarioPorMail(mail);
		cerrarTransaccion();
		return user != null;
	}

	private boolean ExistenciaCuil(String cuil_cuit) {
		iniciarTransaccion();
		Usuario user = dao.getUsuarioPorCuil(cuil_cuit);
		cerrarTransaccion();
		return user != null;
	}

	private String generarMensaje(boolean userName, boolean mail, boolean cuil) {

		String mensaje = "";

		if (userName)
			mensaje += "userName existente\n";
		if (mail)
			mensaje += "mail existente\n";
		if (cuil)
			mensaje += "CUIL/CUIT existente\n";

		return mensaje;
	}

	public Pair<Boolean, String> loguear(String userName, String password) {
		String userNameMin = userName.toLowerCase();
		String passwordMin = password.toLowerCase();
		if (ExisteUserName(userNameMin)) {
			if (existeCoincidencia(userNameMin, passwordMin)) {
				return new Pair<Boolean, String>(true, "Login exitoso");
			} else {
				return new Pair<Boolean, String>(false, "Login fallido\nUsuario y/o password invalido/s");
			}
		} else {
			return new Pair<Boolean, String>(false, "Loguin fallido\nUsuario inexistente");
		}
	}

	private boolean existeCoincidencia(String userName, String password) {
		iniciarTransaccion();
		Usuario user = dao.getUsuarioPorUserName(userName);
		cerrarTransaccion();
		if (password.equals(user.getPassword())) {
			setLogueado(clonarUsuario(user));
			return true;
		}
		return false;
	}

	private Usuario clonarUsuario(Usuario user) {
		Usuario usuario = new Usuario();
		usuario.setId(user.getId());
		usuario.setUserName(user.getUserName());
		usuario.setPassword(user.getPassword());
		usuario.setMail(user.getMail());
		usuario.setCuil_cuit(user.getCuil_cuit());
		return usuario;
	}

}

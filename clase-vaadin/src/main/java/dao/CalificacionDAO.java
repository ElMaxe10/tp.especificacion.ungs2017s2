package dao;

import java.util.List;

import domain.model.Calificacion;

public interface CalificacionDAO extends DAO<Calificacion>{
	public Calificacion getCalificacionPorId(Long idbuscado);
	public List<Calificacion> getTodasLasCalificaciones();
}

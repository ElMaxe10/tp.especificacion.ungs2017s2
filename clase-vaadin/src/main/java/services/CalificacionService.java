package services;

import java.util.List;

import dao.CalificacionDAO;
import daos.impl.CalificacionDAOHibernate;
import domain.model.Calificacion;
import domain.model.Post;
import domain.model.Usuario;

public class CalificacionService {
	
	private CalificacionDAO dao;
	
	public CalificacionService() {
	}
	
	public void iniciarTransaccion(){
		dao = new CalificacionDAOHibernate();
	}
	
	public void cerrarTransaccion(){
		dao.cerrar();
	}
	
	public Calificacion getCalificacionPorId(Long idBuscado){
		iniciarTransaccion();
		Calificacion ret = dao.getCalificacionPorId(idBuscado);
		cerrarTransaccion();
		return ret;
	}
	
	public boolean postFueVotadoPor(Long postId, Long usuarioId){
		List<Calificacion> calificaciones = getCalificaciones();
		for (Calificacion calificacion : calificaciones) {
			if(calificacion.getPost().getId() == postId)
				if(calificacion.getUsuario().getId() == usuarioId)
					return true;
		}
		return false;
	}
	
	private List<Calificacion> getCalificaciones(){
		iniciarTransaccion();
		List<Calificacion> ret = dao.getTodasLasCalificaciones();
		cerrarTransaccion();
		return ret;
	}
	
	public Post calificar(Post post, Usuario usuario, int valor){
		Calificacion calificacion = new Calificacion();
		calificacion.setUsuario(usuario);
		calificacion.setPost(post);
		calificacion.setCalificacion(valor);
		boolean voto = definirVotoPositivo(valor);
		calificacion.setVotoPositivo(voto);
		iniciarTransaccion();
		dao.guardar(calificacion);
		cerrarTransaccion();
		return setVotosEnPost(post, valor, voto);
	}

	private Post setVotosEnPost(Post post, int valor, boolean voto) {
		post.setPuntajeAcumulado(post.getPuntajeAcumulado()+valor);
		if(voto)
			post.setVotosPositivos(post.getVotosPositivos()+1);
		else
			post.setVotosNegativos(post.getVotosNegativos()+1);
		double promedio = post.getPuntajeAcumulado()/(post.getVotosPositivos()+post.getVotosNegativos());
		post.setPuntajePromedio(promedio);
		return post;
	}

	private boolean definirVotoPositivo(int valor) {
		if(valor <= 2)
			return false;
		return true;
	}
}

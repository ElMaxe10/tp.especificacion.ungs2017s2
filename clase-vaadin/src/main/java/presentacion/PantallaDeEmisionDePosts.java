package presentacion;

import java.util.ArrayList;
import java.util.List;
import services.PostService;
import com.vaadin.event.UIEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import domain.model.Post;

@SuppressWarnings("serial")
public class PantallaDeEmisionDePosts extends VerticalLayout implements View {
	protected static final String NAME = "emision-posts";

	private Button volver = new Button("Volver");

	private PostService servicio = new PostService();
	private List<Post> posts;

	private VerticalLayout vertical = new VerticalLayout();

	public PantallaDeEmisionDePosts() {
		addStyleName("fondo");
		construirLayout();
		clickBotones();
	}

	private void construirLayout() {
		this.addComponent(vertical);
		this.addComponent(volver);
	}

	public void iniciar() {
		posts = servicio.getTodosLosPostOrdenadoPorPuntuacion();
		refrescarPantalla();
	}

	private void refrescarPantalla() {

		enviarPostAdibujar();
		regenerarPosts();
		UI.getCurrent().setPollInterval(5000);
		UI.getCurrent().addPollListener(new UIEvents.PollListener() {
			@Override
			public void poll(UIEvents.PollEvent event) {
				enviarPostAdibujar();
				regenerarPosts();
			}
		});
	}

	public void stopTimer() {
		UI.getCurrent().setPollInterval(0);
	}

	private void enviarPostAdibujar() {
		List<Post> postsAux = new ArrayList<Post>();
		for (int i = 0; i <= 4; i++) {
			postsAux.add(posts.get(i));
		}
		dibujadorDePost(postsAux);
	}

	private void dibujadorDePost(List<Post> postAdibujar) {
		vertical.removeAllComponents();
		for (Post post : postAdibujar) {
			String texto = post.getTexto() + " Ratings: " + post.getPuntajePromedio();
			System.out.println(texto);
			Label postCompleto = new Label(texto);
			postCompleto.addStyleName("emisionPost");
			postCompleto.setCaption(post.getUserName());
			vertical.addComponent(postCompleto);
		}
		System.out.println();
		System.out.println();
	}

	private void regenerarPosts() {
		List<Post> postsAux = new ArrayList<Post>();
		for (int i = 0; i <= 4; i++) {
			postsAux.add(posts.get(0));
			posts.remove(0);
			posts.add(postsAux.get(0));
			postsAux.remove(0);
		}
	}

	private void clickBotones() {
		volver.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				moverHaciaMenuPricipal();
				stopTimer();
			}
		});
	}

	private void moverHaciaMenuPricipal() {
		getUI().getNavigator().navigateTo(MenuPrincipal.NAME);
	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		iniciar();
	}

}

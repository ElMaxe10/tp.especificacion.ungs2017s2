package daos.impl;

import java.util.List;

import dao.UsuarioDAO;
import domain.model.Usuario;

public class UsuarioDAOHibernate extends DAOJPA<Usuario> implements UsuarioDAO {

	// UsuarioDAOJPA dao = new UsuarioDAOJPA();

	@Override
	public Usuario getUsuarioPorId(Long idBuscado) {
		return entityManager.find(Usuario.class, idBuscado);
	}

	@Override
	public Usuario getUsuarioPorUserName(String userNameBuscado) {
		List<Usuario> usuarios = getTodosLosUsuarios();
		Usuario userRet = null;
		for (Usuario usuario : usuarios) {
			if (usuario.getUserName().equals(userNameBuscado))
				return userRet = usuario;
		}
		return userRet;
	}

	@Override
	public Usuario getUsuarioPorCuil(String cuilBuscado) {
		List<Usuario> usuarios = getTodosLosUsuarios();
		Usuario userRet = null;
		for (Usuario usuario : usuarios) {
			if (usuario.getCuil_cuit().equals(cuilBuscado))
				return userRet = usuario;
		}
		return userRet;
	}

	@Override
	public Usuario getUsuarioPorMail(String mailBuscado) {
		List<Usuario> usuarios = getTodosLosUsuarios();
		Usuario userRet = null;
		for (Usuario usuario : usuarios) {
			if (usuario.getMail().equals(mailBuscado))
				return userRet = usuario;
		}
		return userRet;
	}

	@Override
	public List<Usuario> getTodosLosUsuarios() {
		entityManager.getTransaction().begin();
		List<Usuario> result = entityManager.createQuery("from Usuario", Usuario.class).getResultList();
		entityManager.getTransaction().commit();
		return result;
	}

}

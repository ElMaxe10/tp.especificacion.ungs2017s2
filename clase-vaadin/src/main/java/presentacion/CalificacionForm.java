package presentacion;

import services.PostService;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

import domain.model.Post;

@SuppressWarnings("serial")
public class CalificacionForm extends FormLayout implements ClickListener {
	private Button votar = new Button("Votar", this);
	private Button cancel = new Button("Cancelar", this);
	private ComboBox calificacion = new ComboBox();

	private Post post;
	private PostService servicio;

	public CalificacionForm() {
		configureComponents();
		buildLayout();
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public void setServicio(PostService servicio) {
		this.servicio = servicio;
	}

	private void configureComponents() {
		votar.setStyleName(ValoTheme.BUTTON_PRIMARY);
		votar.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		setVisible(false);
		for (int i = 1; i <= 5; i++) {
			calificacion.addItem(i);
		}
	}

	private void buildLayout() {
		calificacion.setCaption("Calificacion");
		setSizeUndefined();
		setMargin(true);
		HorizontalLayout actions = new HorizontalLayout(votar, cancel);
		actions.setSpacing(true);
		addComponents(calificacion, actions);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == cancel) {
			this.setVisible(false);
		}

		if (event.getButton() == votar) {
			servicio.calificarPost(post, servicio.getLogueado(), (int) calificacion.getValue());
			this.setVisible(false);
			getUI().getVerPost().refreshPost();
			calificacion.setValue(null);
		}

	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}
}

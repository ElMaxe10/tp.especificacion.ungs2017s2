package dao;

public interface DAO<T> {

	public T guardar(T t);

	public void cerrar();

	public void eliminar(T t);

}

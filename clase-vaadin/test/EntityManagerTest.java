import java.util.Date;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.PostDAO;
import daos.impl.PostDAOHibernate;
import domain.model.Post;
import services.PostService;

public class EntityManagerTest {
	private static EntityManagerFactory entityManagerFactory;
	static PostDAO postDao = new PostDAOHibernate();
	PostService postService = new PostService();

	Post p1 = new Post(new Date(), "soy texto del post1", null);
	Post p2 = new Post(new Date(), "soy texto del post2", null);

	@Test
	public void postDAOtest() {
		postDao.guardar(p1);
	}

	@Test
	public void postServicetest() {
		postService.guardar("hola");
		postService.guardar(p2);
	}

	@BeforeClass
	public static void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("com.vaadin.tp.daos.jpa");
	}

	@AfterClass
	public static void tearDown() throws Exception {
		entityManagerFactory.close();
		postDao.getPosts();
	}

}

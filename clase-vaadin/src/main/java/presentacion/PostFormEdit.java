package presentacion;

import services.PostService;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.themes.ValoTheme;

import domain.model.Post;

@SuppressWarnings("serial")
public class PostFormEdit extends FormLayout implements ClickListener {

	private Button remove = new Button("Eliminar", this);
	private Button cancel = new Button("Cancelar", this);
	private Button save = new Button("Guardar", this);
	private final TextArea postAeditar = new TextArea();

	private Post post;
	

	private PostService servicio;
	private Label error = new Label();
	private Label counter = new Label();

	public PostFormEdit(PostService servicio) {
		this.servicio = servicio;
		configureComponents();
		buildLayout();
	}
	
	public void setPost(Post post) {
		this.post = post;
	}

    private void configureComponents() {
        
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        
        setVisible(false);
        postAeditar.setWidth("320px");
        postAeditar.setMaxLength(300);
        
 // Counter for input length
//        counter.setVisible(false);
        counter.addStyleName("labelContador");
        counter.setValue(postAeditar.getValue().length() +
                         " de " + postAeditar.getMaxLength());

        // Display the current length interactively in the counter
        postAeditar.addTextChangeListener(new TextChangeListener() {
            public void textChange(TextChangeEvent event) {
                int len = event.getText().length();
                counter.setValue(len + " de " + postAeditar.getMaxLength());
//                counter.setVisible(true);
            }
        });

        // The lazy mode is actually the default
        postAeditar.setTextChangeEventMode(TextChangeEventMode.EAGER);
        
        error.setWidth("320px");
        error.addStyleName("v-label-failure");
        error.setValue("El post debe tener al menos un caracter y un maximo de 300");
        error.setVisible(false);
    }

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);
		HorizontalLayout actions = new HorizontalLayout(save, cancel, remove);
		actions.setSpacing(true);
		addComponents(actions, postAeditar,counter, error);
	}

    public void removePost(Post postRemove){
    	post = new Post();
    	post.setId(postRemove.getId());
    }
    
    void edit(Post post) {
    	setVisible(true);
        this.post = post;
        postAeditar.setValue(this.post.getTexto());
    }
    
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == remove) {
			servicio.eliminarPost(post.getId());
//			counter.setVisible(false);
			this.setVisible(false);
			getUI().getCreacionPost().refreshPost();
		}
		if (event.getButton() == cancel) {
//			counter.setVisible(false);
			this.setVisible(false);
		}
		if (event.getButton() == save) {
			
			String textoNuevo = postAeditar.getValue();
			if(!textoNuevo.equals("") && textoNuevo.length()<=300){
				servicio.editarTextoPost(post, textoNuevo);
				getUI().getCreacionPost().refreshPost();
//				counter.setVisible(false);
				this.setVisible(false);
			}
			else{
				Notification.show("El post debe tener al menos un caracter y un maximo de 300");
				error.setVisible(true);
			}
		}
	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

}

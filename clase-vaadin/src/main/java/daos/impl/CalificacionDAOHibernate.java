package daos.impl;

import java.util.List;

import dao.CalificacionDAO;
import domain.model.Calificacion;

public class CalificacionDAOHibernate extends DAOJPA<Calificacion> implements CalificacionDAO{

	@Override
	public Calificacion getCalificacionPorId(Long idbuscado) {
		return entityManager.find(Calificacion.class, idbuscado);
	}

	@Override
	public List<Calificacion> getTodasLasCalificaciones() {
		entityManager.getTransaction().begin();
		List<Calificacion> result = entityManager.createQuery("from Calificacion", Calificacion.class).getResultList();
		entityManager.getTransaction().commit();
		return result;
	}

}

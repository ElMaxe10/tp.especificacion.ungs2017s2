package daos.impl;

import javax.persistence.EntityManager;

import dao.DAO;

public class DAOJPA<T> implements DAO<T> {

	protected EntityManager entityManager;

	public DAOJPA() {
		entityManager = EntityManagers.createEntityManager();
	}

	public T guardar(T t) {
		entityManager.getTransaction().begin();
		T persistent = entityManager.merge(t);
		entityManager.getTransaction().commit();
		return persistent;
	}

	public void cerrar() {
		entityManager.close();
	}

	public void eliminar(T t) {
		entityManager.getTransaction().begin();
		entityManager.remove(t);
		entityManager.getTransaction().commit();
	}

}

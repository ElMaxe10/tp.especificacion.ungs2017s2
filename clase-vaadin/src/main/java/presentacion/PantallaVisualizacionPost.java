package presentacion;

import java.util.List;

import services.PostService;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

import domain.model.Post;
import domain.model.Usuario;

@SuppressWarnings("serial")
public class PantallaVisualizacionPost extends VerticalLayout implements View {
	protected static final String NAME = "ver-posts";

	// Componentes visuales
	private TextField filtro = new TextField();
	private Grid listaPosts = new Grid();
	private Button votarPost = new Button("Votar post");
	private Button btnVolver = new Button("Volver");
	private Label verPost = new Label();
	private CalificacionForm calificacionForm = new CalificacionForm();

	private PostService servicio;
	private Usuario logueado;
	private Post post;

	public PantallaVisualizacionPost() {
		addStyleName("fondo");
		
		construirLayout();
		editarListaPosts();
		clickBotones();
	}

	public Usuario getLogueado() {
		return logueado;
	}

	public void setLogueado(Usuario logueado) {
		this.logueado = logueado;
	}

	public void iniciarTodo() {
		servicio = new PostService(logueado);
		configurarComponentes();
	}

	private void editarListaPosts() {
		listaPosts.setContainerDataSource(new BeanItemContainer<Post>(Post.class));
		listaPosts.setColumnOrder("usuario", "texto", "votosPositivos", "votosNegativos", "fechaCreacion");
		listaPosts.removeColumn("id");
		listaPosts.removeColumn("userName");
		listaPosts.removeColumn("puntajeAcumulado");
		listaPosts.removeColumn("puntajePromedio");

		listaPosts.getColumn("votosPositivos").setMaximumWidth(50);
		listaPosts.getColumn("votosNegativos").setMaximumWidth(50);
		listaPosts.getColumn("usuario").setMaximumWidth(100);
		listaPosts.getColumn("fechaCreacion").setMaximumWidth(100);
		listaPosts.getColumn("texto").setWidth(750);
	}

	private void configurarComponentes() {
		votarPost.setEnabled(false);
		
		verPost.setVisible(false);
		verPost.addStyleName("emisionPost");
		
		filtro.setInputPrompt("Filtrar post...");

		filtro.addTextChangeListener(new TextChangeListener() {
			@Override
			public void textChange(TextChangeEvent event) {
				refreshPost(event.getText());
			}
		});

		// Esto sirve para captar el objeto al seleccionar una fila del grid
		listaPosts.addSelectionListener(new SelectionListener() {
			@Override
			public void select(SelectionEvent event) {
				Post seleccionado = (Post) listaPosts.getSelectedRow();
				if (seleccionado != null) {
					post = (Post) listaPosts.getSelectedRow();
					mostrarPost(post.getTexto());
					if (!esPostDelLogueado(seleccionado) && !yaFueCalificadoPorLogueado(seleccionado))
						votarPost.setEnabled(true);
					else
						votarPost.setEnabled(false);
				} else {
					mostrarPost("");
					votarPost.setEnabled(false);
					post = null;
				}
			}
		});
		refreshPost();
	}

	private boolean esPostDelLogueado(Post seleccionado) {
		if (seleccionado.getUsuario().getId() == getLogueado().getId())
			return true;
		return false;
	}

	private boolean yaFueCalificadoPorLogueado(Post seleccionado) {
		return servicio.getCalificacionService().postFueVotadoPor(seleccionado.getId(), getLogueado().getId());
	}

	private void construirLayout() {
		HorizontalLayout acciones = new HorizontalLayout(filtro, votarPost, btnVolver);
		filtro.setWidth("100%");
		acciones.setWidth("100%");
		acciones.setExpandRatio(filtro, 1);
		acciones.addStyleName("fondo2");

		VerticalLayout left = new VerticalLayout(acciones, listaPosts,verPost);
		listaPosts.setSizeFull();
		left.setExpandRatio(listaPosts, 1);
		left.addStyleName("fondo2");

		HorizontalLayout mainLayout = new HorizontalLayout(left, calificacionForm);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(left, 2);

		this.addComponent(mainLayout);
	}

	public void refreshPost() {
		refreshPost(filtro.getValue());
	}

	private void refreshPost(String stringFilter) {
		List<Post> posts = servicio.buscarTodosLosPost(stringFilter, "todos");
		listaPosts.setContainerDataSource(new BeanItemContainer<Post>(Post.class, posts));
	}

	private void clickBotones() {
		votarPost.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				calificacionForm.setServicio(servicio);
				calificacionForm.setPost(post);
				calificacionForm.setVisible(true);
			}
		});

		btnVolver.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				moverHaciaMenuPricipal();
			}
		});
	}

	private void moverHaciaMenuPricipal() {
		getUI().getNavigator().navigateTo(MenuPrincipal.NAME);
	}

	private void mostrarPost(String texto) {
		if (!texto.equals(null)) {
			verPost.setValue(texto);
			verPost.setVisible(true);
		} else {
			verPost.setValue("");
			verPost.setVisible(false);
		}
	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
	}

}

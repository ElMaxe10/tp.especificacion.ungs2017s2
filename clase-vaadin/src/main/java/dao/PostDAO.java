package dao;

import java.util.List;

import domain.model.Post;

public interface PostDAO extends DAO<Post> {

	public long cantidadPost();

	public Post getPostPorId(Long idbuscado);

	public List<Post> getTodosLosPost();

	public void editarPost(Post p);

}

package services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;









import dao.PostDAO;
import daos.impl.PostDAOHibernate;
import domain.model.Post;
import domain.model.Usuario;

public class PostService {

	private PostDAO dao;
	private Usuario logueado;
	private CalificacionService servicioCalificacion = new CalificacionService();
	
	public PostService(Usuario logueado) {
		setLogueado(logueado);
	}

	public PostService() {
		// TODO Auto-generated constructor stub
	}

	public void iniciarTransaccion(){
		dao = new PostDAOHibernate();
	}
	
	public void cerrarTransaccion(){
		dao.cerrar();
	}

	public void crearPost(Post postNuevo) {
		iniciarTransaccion();
		postNuevo.setFechaCreacion(new Date());
		System.out.println(postNuevo);
		dao.guardar(postNuevo);
		cerrarTransaccion();
	}
	
	public void editarTextoPost(Post postEditar, String textoNuevo) {
		Post editar = buscarPostPorId(postEditar.getId());
		editar.setTexto(textoNuevo);
		iniciarTransaccion();
		dao.guardar(editar);
		cerrarTransaccion();
	}
	
	public void calificarPost(Post post, Usuario usuario, int valor){
		editarCalificacionPost(servicioCalificacion.calificar(post, usuario, valor));
	}
	
	private void editarCalificacionPost(Post postEditar){
		Post editar = buscarPostPorId(postEditar.getId());
		editar = postEditar;
		iniciarTransaccion();
		dao.guardar(editar);
		cerrarTransaccion();
	}

	public List<Post> buscarTodosLosPost(String filtro, String forma) {
		HashMap<Long, Post> mapPost = new HashMap<Long, Post>();
		List<Post> listaPost = null;
		if(forma.equals("usuario"))
			listaPost = buscarTodosLosPostPorUsuario();
		if(forma.equals("todos")){
			listaPost = getTodosLosPost();
		}
			
		for (Post post : listaPost) {
			mapPost.put(post.getId(), post);
		}
		ArrayList<Post> ret = buscarPorFiltro(filtro, mapPost);
		return ret;
	}
	
	private List<Post> buscarTodosLosPostPorUsuario(){		
		List<Post> todosLosPost = getTodosLosPost();
		List<Post> ret = new ArrayList<Post>();
		Long logueado = getLogueado().getId();
		for (Post post : todosLosPost) {
			System.out.println(post);
			System.out.println(logueado);
			if(post.getUsuario().getId() == logueado)
				ret.add(post);
		}
		return ret;
	}
	
	private ArrayList<Post> buscarPorFiltro(String filtro, HashMap<Long, Post> mapPost) {
		ArrayList<Post> postClonados = new ArrayList<Post>();
		for (Post post : mapPost.values()) {
            boolean passesFilter = (filtro == null || filtro
			        .isEmpty())
			        || post.toString().toLowerCase()
			                .contains(filtro.toLowerCase());
			if (passesFilter) {
			    postClonados.add(postClone(post));
			}
        }
        Collections.sort(postClonados, new Comparator<Post>() {

            @Override
            public int compare(Post o1, Post o2) {
                return (int) (o2.getId() - o1.getId());
            }
        });
        return postClonados;
		
	}

	private Post postClone(Post post) {
		Post clone = new Post();
		clone.setFechaCreacion(post.getFechaCreacion());
		clone.setId(post.getId());
		clone.setTexto(post.getTexto());
		clone.setUserName(post.getUserName());
		clone.setUsuario(post.getUsuario());
		clone.setVotosPositivos(post.getVotosPositivos());
		clone.setVotosNegativos(post.getVotosNegativos());
		clone.setPuntajeAcumulado(post.getPuntajeAcumulado());
		clone.setPuntajePromedio(post.getPuntajePromedio());
		return clone;
	}
	
	public List<Post> getTodosLosPost(){
		iniciarTransaccion();
		List<Post> ret = dao.getTodosLosPost();
		cerrarTransaccion();
		return ret;
	}
	
	public List<Post> getTodosLosPostOrdenadoPorPuntuacion(){
		List<Post> posts = getTodosLosPost();
		Collections.sort(posts, new Comparator<Post>() {

            @Override
            public int compare(Post o1, Post o2) {
            	return new Double(o2.getPuntajePromedio()).compareTo(new Double(o1.getPuntajePromedio()));
            }
        });
		return posts;
	}

	public Post buscarPostPorId(Long id) {
		iniciarTransaccion();
		Post post = dao.getPostPorId(id);
		cerrarTransaccion();
		return post;
	}
	
	public void eliminarPost(Long id){
		iniciarTransaccion();
		dao.eliminar(buscarPostPorId(id));
		cerrarTransaccion();
	}

	public Usuario getLogueado() {
		return logueado;
	}

	public void setLogueado(Usuario logueado) {
		this.logueado = logueado;
	}
	
	public CalificacionService getCalificacionService() {
		return servicioCalificacion;
	}

}
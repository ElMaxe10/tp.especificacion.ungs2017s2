package presentacion;

import services.UsuarioService;

import com.google.web.bindery.requestfactory.server.Pair;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.PasswordField;

import domain.model.Usuario;

@SuppressWarnings("serial")
public class PantallaLogin extends VerticalLayout implements View {
	protected static final String NAME = "";
	
	// componentes visuales
	private VerticalLayout layout = new VerticalLayout();
	private TextField userName = new TextField("Escriba su usuario");
	private PasswordField psw = new PasswordField("Escriba su password");
	private Button btnLoguear = new Button("loguear");
	private Button btnRegistro = new Button("Registro");

	private UsuarioService usuarioService = new UsuarioService();
	private Usuario logueado;

	public PantallaLogin() {
		addStyleName("fondo");
		construirLayout();
		clickBotones();
	}

	public Usuario getLogueado() {
		return logueado;
	}

	public void setLogueado(Usuario usuario) {
		logueado = usuario;
	}

	private void construirLayout() {
//		userName.setIcon(new ThemeResource("clase-vaadin/icons/user.png"));
		setSizeFull();
		addComponent(layout);
		layout.setMargin(true);
		layout.setSpacing(true);
		userName.setValue("");
		psw.setValue("");
		HorizontalLayout botones = new HorizontalLayout();
		botones.addComponents(btnLoguear,btnRegistro);
		layout.addComponents(userName,psw,botones);
	}

	private void clickBotones() {
		btnLoguear.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				String userNameAux =userName.getValue();
				String pswAux =psw.getValue();
				if(!userNameAux.equals("") && !pswAux.equals("")){
					Pair<Boolean, String> tupla = usuarioService.loguear(userNameAux, pswAux);
					if (tupla.getA()) {
						limpiarCampos();
						Notification.show(tupla.getB());
						setLogueado(usuarioService.getLogueado());
						compartirLogueado();
						moverHaciaMenuPrincipal();
					} 
					else {
						psw.setValue("");
						Notification.show(tupla.getB());
					}
				}
				else{
					Notification.show("Escriba su usuario y/o contrase�a");
				}
			}
		});

		btnRegistro.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				moverHaciaPantallaDeRegistro();
			}
		});
	}

	private void compartirLogueado() {
		getUI().getCreacionPost().setLogueado(getLogueado());
		getUI().getVerPost().setLogueado(getLogueado());
	}

	private void moverHaciaMenuPrincipal() {
		getUI().getNavigator().navigateTo(MenuPrincipal.NAME);
	}

	private void moverHaciaPantallaDeRegistro() {
		getUI().getNavigator().navigateTo(PantallaRegistro.NAME);
	}

	private void limpiarCampos() {
		userName.setValue("");
		psw.setValue("");
	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
	}

}

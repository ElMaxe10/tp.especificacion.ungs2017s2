package domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	private Long id;
	
	private String userName;
	private String password;
	private String mail;
	private String cuil_cuit;
	private int prestigio = 0;
	private int puntosPrestigio = 30;
	
	public Usuario() {
	}
	
	public Usuario(String userName, String password, String mail, String cuil_cuit){
		setUserName(userName);
		setPassword(password);
		setMail(mail);
		setCuil_cuit(cuil_cuit);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getCuil_cuit() {
		return cuil_cuit;
	}
	public void setCuil_cuit(String cuil_cuit) {
		this.cuil_cuit = cuil_cuit;
	}
	public int getPrestigio() {
		return prestigio;
	}
	public void setPrestigio(int prestigio) {
		this.prestigio = prestigio;
	}
	public int getPuntosPrestigio() {
		return puntosPrestigio;
	}
	public void setPuntosPrestigio(int puntosPrestigio) {
		this.puntosPrestigio = puntosPrestigio;
	}

	@Override
	public String toString() {
//		String ret = "Usuario [userName=" + userName + ", mail=" + mail
//		+ ", cuil_cuit=" + cuil_cuit + ", prestigio=" + prestigio + "]";
		String ret = ""+getUserName();
		return ret;
	}
	
	
	
}

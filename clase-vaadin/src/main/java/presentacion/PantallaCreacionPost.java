package presentacion;

import services.PostService;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import domain.model.Post;
import domain.model.Usuario;

@SuppressWarnings("serial")
public class PantallaCreacionPost extends VerticalLayout implements View {
	protected static final String NAME = "crear-posts";

	// Componentes visuales de la pantalla
	private TextField filtro = new TextField();
	private Grid listaPosts = new Grid();
	private Button nuevoPost = new Button("Nuevo post");
	private Button editarPost = new Button("Editar");
	private Button btnVolver = new Button("Volver");
	private Label verPost = new Label();
	// Pantallas extras
	private PostForm postForm;
	private PostFormEdit postFormEdit;
	// Componentes externos
	private PostService servicio;
	private Post postEditar;
	private Usuario logueado;

	public PantallaCreacionPost() {
		addStyleName("fondo");
		editarListaPost();
		clickBotones();
	}

	public void iniciarTodo() {
		iniciarVariables();
		construirLayout();
		configurarComponentes();
	}

	private void editarListaPost() {
		listaPosts.setContainerDataSource(new BeanItemContainer<Post>(Post.class));
		listaPosts.setColumnOrder("texto", "votosPositivos", "votosNegativos", "fechaCreacion");
		listaPosts.removeColumn("id");
		listaPosts.removeColumn("userName");
		listaPosts.removeColumn("usuario");
		listaPosts.removeColumn("puntajeAcumulado");
		listaPosts.removeColumn("puntajePromedio");

		listaPosts.getColumn("votosPositivos").setMaximumWidth(50);
		listaPosts.getColumn("votosNegativos").setMaximumWidth(50);
		listaPosts.getColumn("fechaCreacion").setMaximumWidth(150);
		listaPosts.getColumn("texto").setMaximumWidth(1500);
		listaPosts.getColumn("texto").setWidth(900);
	}

	private void iniciarVariables() {
		servicio = new PostService(logueado);
		postForm = new PostForm(servicio);
		postFormEdit = new PostFormEdit(servicio);
	}

	private void configurarComponentes() {
		editarPost.setEnabled(false);
		verPost.setVisible(false);
		verPost.setReadOnly(true);
        filtro.setInputPrompt("Filtrar post...");
        
        filtro.addTextChangeListener(new TextChangeListener() {
            @Override
            public void textChange(TextChangeEvent event) {
            	refreshPost(event.getText());
            }
        });        
        
//        Esto sirve para captar el objeto al seleccionar una fila del grid
        listaPosts.addSelectionListener(new SelectionListener() {
	        @Override
	        public void select(SelectionEvent event) {
	        	if((Post)listaPosts.getSelectedRow() != null){
	        		postEditar = (Post)listaPosts.getSelectedRow();
	        		mostrarPost(postEditar.getTexto());
	        		editarPost.setEnabled(true);
	        	}
	        	else{
	        		mostrarPost("");
	        		editarPost.setEnabled(false);
	        	}
	        }
    	});
        refreshPost();
	}

	private void construirLayout() {
		verPost.addStyleName("emisionPost");

		HorizontalLayout acciones = new HorizontalLayout(filtro, nuevoPost, editarPost, btnVolver);
		acciones.setWidth("100%");
		filtro.setWidth("100%");
		acciones.setExpandRatio(filtro, 1);

		VerticalLayout left = new VerticalLayout(acciones, listaPosts,verPost);
		left.setSizeFull();
		listaPosts.setSizeFull();
		left.setExpandRatio(listaPosts, 1);

		HorizontalLayout mainLayout = new HorizontalLayout(left, postForm, postFormEdit);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(left, 1);

		this.addComponent(mainLayout);

	}

	public void refreshPost() {
		refreshPost(filtro.getValue());
	}

	private void refreshPost(String stringFilter) {
		listaPosts.setContainerDataSource(
				new BeanItemContainer<Post>(Post.class, servicio.buscarTodosLosPost(stringFilter, "usuario")));
		// postForm.setVisible(false);
	}

	private void clickBotones() {
		nuevoPost.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				postForm.setVisible(true);
			}
		});

		editarPost.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if (postEditar != null)
					postFormEdit.setPost(postEditar);
					postFormEdit.edit(postEditar);
			}
		});

		btnVolver.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(MenuPrincipal.NAME);
			}
		});
	}

	private void mostrarPost(String texto) {
		if (!texto.equals(null)) {
			verPost.setValue(texto);
			verPost.setVisible(true);
		} else {
			verPost.setValue("");
			verPost.setVisible(false);
		}
	}

	public Usuario getLogueado() {
		return logueado;
	}

	public void setLogueado(Usuario logueado) {
		this.logueado = logueado;
	}
	
	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// si quiero c�digo que se ejecute al ingresar.
	}

}

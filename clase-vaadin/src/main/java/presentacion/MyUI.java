package presentacion;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Title("MyUI")
@Theme("mytheme")
public class MyUI extends UI {
	private Navigator navigator;
	private MenuPrincipal menu;
	private PantallaRegistro registro;
	private PantallaLogin login;
	private PantallaCreacionPost creacionPost;
	private PantallaVisualizacionPost verPost;
	private PantallaDeEmisionDePosts emisionPosts;

	@Override
	protected void init(VaadinRequest request) {
		getPage().setTitle("Una aplicacion Social en un Mundo Real");

		// Creamos el navegador
		navigator = new Navigator(this, this);
		menu = new MenuPrincipal();
		registro = new PantallaRegistro();
		login = new PantallaLogin();
		creacionPost = new PantallaCreacionPost();
		verPost = new PantallaVisualizacionPost();
		emisionPosts = new PantallaDeEmisionDePosts();

		// Y creamos y registramos las views
		navigator.addView(MenuPrincipal.NAME, menu);
		navigator.addView(PantallaRegistro.NAME, registro);
		navigator.addView(PantallaLogin.NAME, login);
		navigator.addView(PantallaCreacionPost.NAME, creacionPost);
		navigator.addView(PantallaVisualizacionPost.NAME, verPost);
		navigator.addView(PantallaDeEmisionDePosts.NAME, emisionPosts);
	}

	public Navigator getNavigator() {
		return navigator;
	}

	public MenuPrincipal getMenu() {
		return menu;
	}

	public PantallaRegistro getRegistro() {
		return registro;
	}

	public PantallaLogin getLogin() {
		return login;
	}

	public PantallaCreacionPost getCreacionPost() {
		return creacionPost;
	}

	public PantallaVisualizacionPost getVerPost() {
		return verPost;
	}

	public PantallaDeEmisionDePosts getEmisionPosts() {
		return emisionPosts;
	}

	@WebServlet(urlPatterns = "/*")
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
}

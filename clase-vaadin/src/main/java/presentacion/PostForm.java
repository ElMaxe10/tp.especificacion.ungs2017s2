package presentacion;

import services.PostService;

import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.themes.ValoTheme;

import domain.model.Post;
import domain.model.Usuario;

@SuppressWarnings("serial")
public class PostForm extends FormLayout implements ClickListener {

    private Button save = new Button("Guardar", this);
    private Button cancel = new Button("Cancelar", this);
    private final TextArea nuevoPost = new TextArea();
    private Label error = new Label();
    private Label counter = new Label();
    
    private PostService servicio;

	public PostForm(PostService servicio) {
		this.servicio = servicio;
		configureComponents();
		buildLayout();
	}

    private void configureComponents() {
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setVisible(false);
        nuevoPost.setInputPrompt("Escriba su post aqui...");
        nuevoPost.setWidth("320px");
        nuevoPost.setMaxLength(300);
        nuevoPost.addStyleName(".v-textarea-color3");
        
     // Counter for input length
//        counter.setVisible(false);
        counter.addStyleName("labelContador");
        counter.setValue(nuevoPost.getValue().length() +
                         " de " + nuevoPost.getMaxLength());

        // Display the current length interactively in the counter
        nuevoPost.addTextChangeListener(new TextChangeListener() {
            public void textChange(TextChangeEvent event) {
//            	counter.setVisible(true);
                int len = event.getText().length();
                counter.setValue(len + " de " + nuevoPost.getMaxLength());
            }
        });

        // The lazy mode is actually the default
        nuevoPost.setTextChangeEventMode(TextChangeEventMode.EAGER);
        
        error.setWidth("320px");
        error.addStyleName("v-label-failure");
        error.setValue("El post debe tener al menos un caracter y un maximo de 300");
        error.setVisible(false);
    }

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);
		HorizontalLayout actions = new HorizontalLayout(save, cancel);
		actions.setSpacing(true);
        addComponents(actions, nuevoPost,counter,error);
//        addStyleName("fondo2");
    }
    
    void edit(Post post) {
        nuevoPost.setValue(post.getTexto());
    }
    
	@Override
	public void buttonClick(ClickEvent event) {
		if (event.getButton() == save) {
			if(!nuevoPost.getValue().equals("")){
				error.setVisible(false);
				Usuario logueado = getUI().getCreacionPost().getLogueado();
				servicio.crearPost(new Post(nuevoPost.getValue(),logueado, logueado.getUserName()));
				getUI().getCreacionPost().refreshPost();
				nuevoPost.setValue("");
				this.setVisible(false);
//				counter.setVisible(false);
			}
			else{
				Notification.show("El post debe tener al menos un caracter y un maximo de 300");
				error.setVisible(true);
			}
		}
		
		if (event.getButton() == cancel) {
			nuevoPost.setValue("");
			this.setVisible(false);
//			counter.setVisible(false);
		}
	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

}

package presentacion;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class MenuPrincipal extends HorizontalSplitPanel implements View {
	protected static final String NAME = "menuPrincipal";
	private Panel contenedorViews;

	public Panel getContenedorViews() {
		return contenedorViews;
	}

	public MenuPrincipal() {
		addStyleName("fondo");
		setSizeFull();

		final VerticalLayout layout = new VerticalLayout();
		addComponent(layout);
		layout.setMargin(true);
		layout.setSpacing(true);
		layout.setWidth("300px");
		Button btnCrearPost = new Button("Crear Post");
		btnCrearPost.setWidth("300px");
		btnCrearPost.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getCreacionPost().iniciarTodo();
				getUI().getNavigator().navigateTo(PantallaCreacionPost.NAME);
			}
		});
		layout.addComponent(btnCrearPost);

		Button btnVisualizarPosts = new Button("Ver Posts");
		btnVisualizarPosts.setWidth("300px");
		btnVisualizarPosts.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getVerPost().iniciarTodo();
				getUI().getNavigator().navigateTo(PantallaVisualizacionPost.NAME);
			}
		});
		layout.addComponent(btnVisualizarPosts);

		Button btnEmisionPosts = new Button("Emision de Posts");
		btnEmisionPosts.setWidth("300px");
		btnEmisionPosts.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(PantallaDeEmisionDePosts.NAME);
			}
		});
		layout.addComponent(btnEmisionPosts);

		Button btnCerrarSesion = new Button("Cerrar sesion");
		btnCerrarSesion.setWidth("300px");
		btnCerrarSesion.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(PantallaLogin.NAME);
			}
		});
		layout.addComponent(btnCerrarSesion);

	}

	@Override
	public MyUI getUI() {
		return (MyUI) super.getUI();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// si quiero codigo que se ejecute al ingresar.
	}
}

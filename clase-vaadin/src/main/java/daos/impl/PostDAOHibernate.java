package daos.impl;

import java.util.List;

import dao.PostDAO;
import domain.model.Post;

public class PostDAOHibernate extends DAOJPA<Post> implements PostDAO {

	public static PostDAOHibernate dao = new PostDAOHibernate();

	public long cantidadPost() {

		return entityManager.createQuery("SELECT COUNT(c) FROM Post c", Long.class).getSingleResult();
	}

	@Override
	public Post getPostPorId(Long idbuscado) {
		return entityManager.find(Post.class, idbuscado);
	}

	@Override
	public List<Post> getTodosLosPost() {
		entityManager.getTransaction().begin();
		List<Post> result = entityManager.createQuery("from Post", Post.class).getResultList();
		entityManager.getTransaction().commit();
		return result;
	}

	@Override
	public void editarPost(Post p) {
		entityManager.merge(p);
	}

}
